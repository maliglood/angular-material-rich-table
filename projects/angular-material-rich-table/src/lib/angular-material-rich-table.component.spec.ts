import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RichMatTableComponent } from './angular-material-rich-table.component';

describe('ExtendedTableComponent', () => {
  let component: RichMatTableComponent;
  let fixture: ComponentFixture<RichMatTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RichMatTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RichMatTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
