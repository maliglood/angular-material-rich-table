/**
 * Интерфейс опшин для селекта.
 */
export interface ISelectOption<TValue> {
    /**
     * Отображаемое человеко-понятное описание.
     */
    title: string;

    /**
     * Значение пункта.
     */
    value: TValue;
}
