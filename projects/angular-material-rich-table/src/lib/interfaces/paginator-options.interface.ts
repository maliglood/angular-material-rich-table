import { ThemePalette } from '@angular/material';

/**
 * Настройки пагинации.
 */
export interface IPaginatorOptions {
    /**
     * Включение пагинации.
     */
    show: boolean;

    /**
     * Цвет пагинатора.
     */
    color: ThemePalette;

    /**
     * Блокировка пагинатора.
     */
    disabled: boolean;

    /**
     * Сокрытие размера страницы.
     */
    hidePageSize: boolean;

    /**
     * Общее количество данных.
     */
    length: number;

    /**
     * Отображаемая страница.
     */
    pageIndex: number;

    /**
     * Количество данных на странице.
     */
    pageSize: number;

    /**
     * Варианты количества данных на странице.
     */
    pageSizeOptions: number[];

    /**
     * Отображение кнопок "первой" и "последней" страниц.
     */
    showFirstLastButtons: boolean;

    /**
     * Позиционирование пагинатора.
     */
    position: 'left' | 'center' | 'right';
}
