import { ISelectOption } from './';

/**
 * Информация о столбце.
 */
export type ITableColumn = ITableColumnBase
                            | ITableFilterSelectColumn
                            | ITableButtonsColumn
                            | ITableCheckboxFilterSelectColumn
                            | ITableImageColumn;

/**
 * Базовый интерфейс.
 */
export interface ITableColumnBase {
    /**
     * Отображаемое имя столбца.
     */
    title: string;

    /**
     * Название соответствующего поля в объектах строк.
     */
    fieldName: string;

    /**
     * Тип столбца.
     */
    type: 'string' | 'number' | 'checkbox' | 'buttons' | 'image';

    /**
     * Тип фильтрации.
     */
    filterType: 'none' | 'input' | 'select' | 'checkbox';
}

/**
 * Информация о столбце с фильтрацией в виде селекта.
 */
export interface ITableFilterSelectColumn extends ITableColumnBase {
    /**
     * Тип фильтрации.
     */
    filterType: 'select';

    /**
     * Список значений для filterType='select'.
     */
    filterSelectOptions: ISelectOption<string>[];
}

/**
 * Информация о столбце с фильтрацией в виде селекта.
 */
export interface ITableCheckboxFilterSelectColumn extends ITableColumnBase {
    /**
     * Тип столбца.
     */
    type: 'checkbox';

    /**
     * Тип фильтрации.
     */
    filterType: 'select';
}

/**
 * Информация о столбце с кнопками.
 */
export interface ITableButtonsColumn extends ITableColumnBase {
    /**
     * Тип фильтрации.
     */
    filterType: 'none';

    /**
     * Тип столбца.
     */
    type: 'buttons';

    /**
     * Список значений для filterType='select'. Не учитывается, если type='checkbox'.
     */
    filterSelectOptions?: ISelectOption<string>[];

    /**
     * Список кнопок.
     */
    buttons: ITableButton[];
}

/**
 * Информация о столбце с типом изображения.
 */
export interface ITableImageColumn extends ITableColumnBase {
    /**
     * Тип столбца.
     */
    type: 'image';

    /**
     * Тип фильтрации.
     */
    filterType: 'none' | 'select';

    /**
     * Соответствие картинок значениям.
     */
    images: IImageInfo[];
}

/**
 * Список кнопок для type='buttons'.
 */
export interface ITableButton {
    /**
     * Изображение.
     */
    icon: string;

    /**
     * Текст всплывающей подсказки.
     */
    tooltip: string;

    /**
     * Колбэк кнопки.
     */
    callback: Function;

    /**
     * Цвет кнопки.
     */
    color?: MaterialColorType;
}

/**
 * Информация об изобюражении для ITableImageColumn.
 */
export interface IImageInfo {
    /**
     * Значение поля.
     */
    value: string;

    /**
     * Изображение.
     */
    icon: string;

    /**
     * Цвет изображения
     */
    color?: MaterialColorType;

    /**
     * Человеко-понятное описание (для фильтра-селекта).
     */
    title?: string;
}

/**
 * Варианты цвета для Angular Material
 */
export type MaterialColorType = 'primary' | 'accent' | 'warn';
