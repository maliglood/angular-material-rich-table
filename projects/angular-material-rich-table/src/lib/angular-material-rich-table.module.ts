import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule } from '@angular/forms';
import ru from '@angular/common/locales/ru';
import {
  MatTableModule,
  MatFormFieldModule,
  MatSelectModule,
  MatDialogModule,
  MatTabsModule,
  MatInputModule,
  MatCheckboxModule,
  MatIconModule,
  MatListModule,
  MatGridListModule,
  MatTooltipModule,
  MatButtonModule,
  MatPaginatorModule,
  MatToolbarModule
} from '@angular/material';

import { RichMatTableComponent } from './angular-material-rich-table.component';

registerLocaleData(ru);

@NgModule({
  declarations: [
    RichMatTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
    MatTabsModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatTooltipModule,
    MatButtonModule,
    MatPaginatorModule,
    MatToolbarModule
  ],
  exports: [
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
    MatTabsModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatTooltipModule,
    MatButtonModule,
    MatPaginatorModule,
    MatToolbarModule,
    RichMatTableComponent
  ]
})
export class AngularMaterialRichTableModule { }
