import { Component, AfterViewInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { ITableColumn, ISelectOption, ITableFilterSelectColumn, IPaginatorOptions, ITableImageColumn, IImageInfo } from './interfaces';
import { tap } from 'rxjs/operators';

/**
 * Таблица.
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'rich-mat-table',
  templateUrl: './angular-material-rich-table.component.html',
  styleUrls: ['./angular-material-rich-table.component.scss']
})
export class RichMatTableComponent<T> implements AfterViewInit {

  /**
   * Информация о столбцах таблицы.
   */
  private _columns: ITableColumn[];

  /**
   * Исходные данные для таблицы.
   */
  private _sourceData: T[];

  /**
   * Готовые данные для таблицы.
   */
  private _tableData: MatTableDataSource<T>;

  /**
   * Текущие введённые значения фильтра.
   */
  private _filter: { [field: string]: string } = {};

  /**
   * Включение первого столбца с чекбоксами для выделения строк.
   */
  private _useCheckboxColumn = false;

  /**
   * Данные для пагинации.
   */
  public _paginatorOptions: IPaginatorOptions;

  /**
   * Передаваемые данные для таблице.
   */
  @Input()
  set sourceData(value: T[]) {
    this._sourceData = value;
    this._tableData = this._initTableData(value);
    this.checkboxColumnData = value.map(() => false);
    this._paginatorOptions.length = value.length || 0;
    this._paginatorOptions.pageIndex = 0;

    this.setSelectFilters();
  }
  get sourceData(): T[] {
    return this._sourceData;
  }

  /**
   * Передаваемая информация о столбцах таблицы.
   */
  @Input()
  set columns(value: ITableColumn[]) {
    this._columns = value;
    this.displayedColumns = this._columns.map(x => x.fieldName);
    if (this.useCheckboxColumn) {
      this.displayedColumns.unshift(this.checkboxColumnName);
    }
    this.setSelectFilters();
  }
  get columns(): ITableColumn[] {
    return this._columns;
  }

  /**
   * Включение первого столбца с чекбоксами для выделения строк.
   */
  @Input()
  set useCheckboxColumn(value: boolean) {
    this._useCheckboxColumn = value;

    const checkboxColumnNameIndex = this.displayedColumns.findIndex(field => field === this.checkboxColumnName);

    if (this.useCheckboxColumn && checkboxColumnNameIndex < 0) {
      this.displayedColumns.unshift(this.checkboxColumnName);
    } else {
      if (!this.useCheckboxColumn && checkboxColumnNameIndex >= 0) {
        this.displayedColumns.splice(checkboxColumnNameIndex, 1);
      }
    }
  }
  get useCheckboxColumn(): boolean {
    return this._useCheckboxColumn;
  }

  /**
   * Данные для пагинации.
   */
  @Input()
  set paginatorOptions(value: IPaginatorOptions) {
    this._paginatorOptions = this._normalizePaginatorOptions(value);
  }
  get paginatorOptions() {
    return this._paginatorOptions;
  }

  /**
   * Элемент пагинатора.
   */
  @ViewChild(MatPaginator, { static: false })
  public paginator: MatPaginator;

  /**
   * Элемент сортировки.
   */
  @ViewChild(MatSort, { static: false })
  public sort: MatSort;

  /**
   * Готовые данные для таблицы.
   */
  get tableData(): MatTableDataSource<T> {
    return this._tableData;
  }

  /**
   * Название столбца с чекбоками для выделения строк.
   */
  public checkboxColumnName = 'checkboxColumn';

  /**
   * Отображаемые столбцы.
   */
  public displayedColumns: string[] = [];

  /**
   * Информация о включенных столбцах.
   */
  public checkboxColumnData: boolean[] = [];

  /**
   * Значение чекбокса в хедере столбца для выделения всех строк.
   */
  public checkboxColumnHeaderValue = false;

  /**
   * Список опшинов для селекта столбца булевого типа.
   */
  public filterCheckBoxSelectOptions: ISelectOption<string>[] = [
    {
      title: 'Да',
      value: 'true'
    },
    {
      title: 'Нет',
      value: 'false'
    }
  ];

  /**
   * Конструктор.
   */
  constructor() {
    this._paginatorOptions = this._normalizePaginatorOptions();
  }

  /**
   * СОбытие после инициализации компонента.
   */
  ngAfterViewInit() {
    if (!this._tableData.paginator && this.paginator) {
      this._tableData.paginator = this.paginator;
    }
  }

  /**
   * Поколоночная фильтрация инпутом.
   * @param value Значение фильтра.
   * @param fieldName Фильтруемое поле.
   */
  public columnInputFilter(value: string, fieldName: string) {
    this._filter[fieldName] = value;
    this._tableData.filter = value !== undefined ? value.trim() : value;
  }

  /**
   * Поколоночная фильтрация селектом.
   * @param value Значение фильтра.
   * @param fieldName Фильтруемое поле.
   */
  public columnSelectFilter(value: string, fieldName: string) {
    this._filter[fieldName] = value;
    this._tableData.filter = value !== undefined ? value.trim() : value;
  }

  /**
   * Выделить/снять выделение всех строк.
   */
  public selectAll() {
    this.checkboxColumnData.fill(this.checkboxColumnHeaderValue);
  }

  /**
   * Событие на вызов изменения значения выделения одной строки.
   */
  public select() {
    const areAllRowSelected = this.checkboxColumnData.every(x => x);
    this.checkboxColumnHeaderValue = areAllRowSelected;
  }

  /**
   * Возвращает выделенные строки.
   */
  public getSelectedRows(): T[] {
    if (!this._useCheckboxColumn) {
      console.log('Checkbox column is not switched-on');
      return [];
    }

    if (this.checkboxColumnData.length !== this._sourceData.length) {
      console.log('Error of selected data!');
      return [];
    }

    const selectedRows: T[] = [];

    for (let i = 0; i < this._sourceData.length; i++) {
      if (this.checkboxColumnData[i]) {
        selectedRows.push(this._sourceData[i]);
      }
    }

    return selectedRows;
  }

  /**
   * Возвращает объект изображения.
   * @param row Строка таблицы.
   * @param column Настройки колонки.
   */
  public getImage(row: T, column: ITableImageColumn): IImageInfo {
    return column.images.find(x => x.value === row[column.fieldName]);
  }

  /**
   * Непосредственно функция фильтрации.
   * @param data Данные строки.
   */
  private filter = (data: T) => {
    let result = true;

    for (const filterFieldName in this._filter) {
      // tslint:disable-next-line:triple-equals
      if (this._filter[filterFieldName]) {
        const column = this._columns.find((columnInfo) => columnInfo.fieldName === filterFieldName);

        switch (column.type) {
          case 'string':
          case 'image':
            if (!data[filterFieldName].toLowerCase().includes(this._filter[filterFieldName].toLowerCase())) {
              result = false;
            }
            break;
          case 'number':
            if (!data[filterFieldName].toString().includes(this._filter[filterFieldName].toString())) {
              result = false;
            }
            break;
          case 'checkbox':
            const filterValue: boolean = typeof this._filter[filterFieldName] === 'string'
              ? JSON.parse(this._filter[filterFieldName]) as boolean
              : this._filter[filterFieldName] as unknown as boolean;

            if (data[filterFieldName] !== filterValue) {
              result = false;
            }
            break;
        }

        if (!result) {
          break;
        }
      }
    }

    return result;
  }

  /**
   * Настройка селектов фильтрации.
   */
  private setSelectFilters() {
    if (!this.columns || !this.sourceData) { return; }

    for (const column of this.columns) {
      if (column.filterType === 'select') {
        const filterSelectColumn = (column as ITableFilterSelectColumn);

        switch (column.type) {
          case 'checkbox':
            filterSelectColumn.filterSelectOptions = this.filterCheckBoxSelectOptions;
            break;
          case 'image':
            const imageColumn = (column as ITableImageColumn);
            filterSelectColumn.filterSelectOptions = imageColumn.images.map(i => {
              return {
                value: i.value,
                title: i.title || i.value
              };
            });
            break;
          default:
            if (!filterSelectColumn.filterSelectOptions || filterSelectColumn.filterSelectOptions === []) {
              const filterSelectOptions = [];

              for (const row of this.sourceData) {
                // tslint:disable-next-line:triple-equals
                if (!filterSelectOptions.find(option => option == row[column.fieldName])) {
                  const value = row[column.fieldName];
                  filterSelectOptions.push({
                    title: value,
                    value: value
                  });
                }
              }

              filterSelectColumn.filterSelectOptions = filterSelectOptions;
            }
            break;
        }
        if (column.type === 'checkbox') {
          filterSelectColumn.filterSelectOptions = this.filterCheckBoxSelectOptions;
        } else {
          if (!filterSelectColumn.filterSelectOptions || filterSelectColumn.filterSelectOptions === []) {
            const filterSelectOptions = [];

            for (const row of this.sourceData) {
              // tslint:disable-next-line:triple-equals
              if (!filterSelectOptions.find(option => option == row[column.fieldName])) {
                filterSelectOptions.push(row[column.fieldName]);
              }
            }

            filterSelectColumn.filterSelectOptions = filterSelectOptions;
          }
        }
      }
    }
  }

  /**
   * Заполнение недостающих натсроек пагинатора дефолтными.
   * @param paginatorOptions Настройки пагинатора.
   */
  private _normalizePaginatorOptions(paginatorOptions?: IPaginatorOptions) {
    const defaultOptions: IPaginatorOptions = {
      show: true,
      color: 'primary',
      disabled: false,
      hidePageSize: false,
      length: this._sourceData ? this._sourceData.length : 0,
      pageIndex: 0,
      pageSize: 20,
      pageSizeOptions: [10, 20, 50],
      showFirstLastButtons: true,
      position: 'center'
    };

    return Object.assign(defaultOptions, paginatorOptions);
  }

  /**
   * Инициализация данных для отображения.
   * @param fullData Все данные (Без фильтрации).
   */
  private _initTableData(fullData: T[]) {
    const tableData = new MatTableDataSource<T>(fullData);

    if (this.paginator) {
      tableData.paginator = this.paginator;
    }

    if (tableData) {
      tableData.filterPredicate = this.filter;
    }

    return tableData;
  }

}
